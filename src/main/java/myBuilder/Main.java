package myBuilder;

import java.util.LinkedList;
import java.util.List;

/**
 * Stwórz aplikację a w niej klasę GameCharacter która jest postacią w grze. Postać powinna mieć pola:
 name,
 health,
 mana,
 number of points
 Stwórz w niej buildera.
 W mainie stwórz kolekcję postaci (listę lub set) i dodaj do niej kilka takich samych postaci, oraz
 kilka postaci odrobine zmodyfikowanych (punktami lub mana i zyciem).
 */

public class Main {
    public static void main(String[] args) {
        GameCharacter character1 = new GameCharacter.Builder().setName("Leon")
                .setHalth(1000).setMana(20).setNumberOfPoints(1).create();
        GameCharacter character2 = new GameCharacter.Builder().setMana(1000).create();
        GameCharacter character3 = new GameCharacter.Builder().setName("Jan").setHalth(10).setNumberOfPoints(2).create();
        GameCharacter character4 = new GameCharacter.Builder().setName("Jan").setHalth(10).setNumberOfPoints(2).create();

        List<GameCharacter> gameCharacterList = new LinkedList<>();
        gameCharacterList.add(character1);
        gameCharacterList.add(character2);
        gameCharacterList.add(character3);
        gameCharacterList.add(character4);

        for (GameCharacter g: gameCharacterList) {
            System.out.println(g);
        }
    }
}
