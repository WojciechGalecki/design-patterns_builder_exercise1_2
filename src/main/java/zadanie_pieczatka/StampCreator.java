package zadanie_pieczatka;

public class StampCreator {
    private Stamp stamp;

    public Stamp getStamp() {
        return stamp;
    }

    public Stamp createStamp(String userLine){
        try {
            String[] parameters = userLine.split("-");
            int day1 = Integer.parseInt(parameters[0]);
            int day2 = Integer.parseInt(parameters[1]);
            int month1 = Integer.parseInt(parameters[2]);
            int month2 = Integer.parseInt(parameters[3]);
            int year1 = Integer.parseInt(parameters[4]);
            int year2 = Integer.parseInt(parameters[5]);
            int year3 = Integer.parseInt(parameters[6]);
            int year4 = Integer.parseInt(parameters[7]);
            int caseNo = Integer.parseInt(parameters[8]);
            stamp = new Stamp.Builder().setFirstDayNumber(day1)
                    .setSecondDayNumber(day2)
                    .setFirstMonthNumber(month1)
                    .setSecondMonthNumber(month2)
                    .setYearNumber1(year1)
                    .setYearNumber2(year2)
                    .setYearNumber3(year3)
                    .setYearNumber4(year4)
                    .setCaseNumber(caseNo)
                    .create();
        } catch (ArrayIndexOutOfBoundsException e) {
            System.err.println("Wrong date format!!!");
            e.printStackTrace();
        } catch (NumberFormatException ne) {
            System.out.println("Wrong date format!!!");
            ne.printStackTrace();
        }
        return stamp;
    }
}
